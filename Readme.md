My personal website
===================

Hello.

This is the source code of my personal website and blog, including all the content. The live version is available at https://tad-lispy.com/.

I use GNU Make as a build system, so if you want to fork and build it yourself just call:

```shell
make
```

Or start development process with watching and local HTTP server with

```
make run
```

